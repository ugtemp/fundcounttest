package com.alex.example;

import com.google.gson.Gson;
import org.jdatepicker.impl.DateComponentFormatter;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * Created by Shishkov A.V. on 18.09.18.
 */
public class SwingApplication extends JFrame {
	private JDatePickerImpl datePicker;
	private JTextField quantityTextField;
	private JTextField resultTextField;

	public SwingApplication(String title) throws HeadlessException {
		super(title);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addComponentsToPane(this);

		this.pack();
		this.setBounds(0, 0, 480, 250);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private double getUsdPrice(String date) throws IOException {
		URL url = new URL("http://data.fixer.io/api/" + date
				+ "?access_key=6d2c4abd770561642bb00b4893c86a2e&symbols=USD,RUB,EUR");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");

		try (InputStream jsonStream = connection.getInputStream()) {
			Gson gson = new Gson();
			try (InputStreamReader reader = new InputStreamReader(jsonStream)) {
				FinancialQuote quote = gson.fromJson(reader, FinancialQuote.class);
				double usdRate = quote.getRates().get("USD");
				double rubRate = quote.getRates().get("RUB");
				return rubRate * (1 / usdRate);
			}
		}
	}

	private void addComponentsToPane(JFrame pane) {
		pane.setLayout(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(10, 10, 10, 10);

		JLabel buyDateLabel = new JLabel("Дата покупки:");
		buyDateLabel.setHorizontalAlignment(0);
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.WEST;
		pane.add(buyDateLabel, constraints);

		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");

		datePicker =
				new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), p),
						new DateComponentFormatter());
		constraints.gridx = 1;
		constraints.gridy = 0;
		pane.add(datePicker, constraints);

		JLabel quantityLabel = new JLabel("Сумма в Долларах США:");
		constraints.gridx = 0;
		constraints.gridy = 1;
		pane.add(quantityLabel, constraints);

		quantityTextField = new JTextField("100");
		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		quantityTextField.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				char keyChar = e.getKeyChar();
				if (!Character.isDigit(keyChar)
						|| (keyChar == KeyEvent.VK_BACK_SPACE)
						|| (keyChar == KeyEvent.VK_DELETE)) {
					e.consume();
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {

			}
		});
		pane.add(quantityTextField, constraints);

		JButton button = new JButton("Расчёт");
		constraints.gridx = 1;
		constraints.gridy = 2;
		constraints.insets = new Insets(10, 10, 10, 10);
		button.setBounds(new Rectangle(100, 25));
		button.addActionListener(e -> {
			if (!validateInputData()) return;

			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
				Date oldDate = dateFormat.parse(datePicker.getJFormattedTextField().getText());
				dateFormat = new SimpleDateFormat("yyyy-MM-dd");

				double oldPrice = getUsdPrice(dateFormat.format(oldDate));
				double newPrice = getUsdPrice("latest");
				double difference = (newPrice - oldPrice) * Integer.parseInt(quantityTextField.getText());

				NumberFormat numberFormat = new DecimalFormat("##.##");
				resultTextField.setText(numberFormat.format(difference) + " руб.");
			} catch (ParseException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(this, "Ошибка получения данных по курсу валют. " +
						"Попробуйте повторить попытку", "Получение курса валют", JOptionPane.ERROR_MESSAGE);
			}
		});

		pane.add(button, constraints);

		JLabel resultLabel = new JLabel("Результат:");
		constraints.gridx = 0;
		constraints.gridy = 3;
		pane.add(resultLabel, constraints);

		resultTextField = new JTextField();
		resultTextField.setEditable(false);
		constraints.gridx = 1;
		constraints.gridy = 3;
		pane.add(resultTextField, constraints);

		this.setResizable(false);
	}

	private boolean validateInputData() {
		String date = this.datePicker.getJFormattedTextField().getText();

		if (date == null || date.isEmpty()) {
			JOptionPane.showMessageDialog(this,
					"Заполните дату покупки", "Валидация данных", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		String quantity = this.quantityTextField.getText();

		if (quantity == null || quantity.isEmpty()) {
			JOptionPane.showMessageDialog(this,
					"Заполните сумму покупки", "Валидация данных", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		try {
			int quantityValue = Integer.parseInt(quantity);

			if (quantityValue < 0) throw new NumberFormatException();
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this,
					"Сумма покупки должна быть положительным целым числом", "Валидация данных",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new SwingApplication("Расчёт прибыли/убытка покупки " +
				"долларов (США)"));
	}
}

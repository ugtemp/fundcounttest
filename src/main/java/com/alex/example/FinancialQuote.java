package com.alex.example;

import java.util.Date;
import java.util.Map;

/**
 * Created by Shishkov A.V. on 18.09.18.
 */
public class FinancialQuote {
	private boolean success;
	private Long timestamp;
	private Date timeStampValue;
	private String base;
	private String date;
	private Map<String, Double> rates;

	public FinancialQuote() {
	}

	public FinancialQuote(boolean success, Long timestamp, String base, String date) {
		this.success = success;
		this.timestamp = timestamp;
		this.base = base;
		this.date = date;
	}

	public FinancialQuote(boolean success, Long timestamp, String base, String date, Map<String, Double> rates) {
		this.success = success;
		this.timestamp = timestamp;
		this.base = base;
		this.date = date;
		this.rates = rates;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Map<String, Double> getRates() {
		return rates;
	}

	public void setRates(Map<String, Double> rates) {
		this.rates = rates;
	}

	public Date getTimeStampValue() {
		return new Date(this.timestamp * 1000);
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
